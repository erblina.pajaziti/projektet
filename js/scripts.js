$(function(){
    //Ie + Ie number
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
       $('body').addClass('ie').addClass('ie'+parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
    }
    
})
;$(window).resize(function(){
     if (Modernizr.mq('(max-width: 767px)')) {
       $('html').addClass("mobile-menu-js").removeClass("desktop-menu-js");
       } else {
         $('html').addClass("desktop-menu-js").removeClass("mobile-menu-js");
       }
 });
if (!$('html').hasClass('ie8')) {
       var wow = new WOW({
           boxClass:     'wow',      // default
           animateClass: 'animated', // default
           offset:       0,          // default
           obile:       true,       // default
           live:         true        // default
       });
       wow.init();
 }
validate = {
    init: function() {
        if ($('.parsleyform').length) {
            $('.parsleyform').each(function(e) {
                var thisi = $(this);
                thisi.parsley({
                    errors: {
                        classHandler: function(elem, isRadioOrCheckbox) {
                            //console.log($(elem));
                            if (isRadioOrCheckbox) {
                                return $(elem).closest('.checkbox_group');
                            }
                            else if($(elem).hasClass('selectbox')){
                                return $(elem).closest('.selectparent');
                            }
                        },
                        container: function(element, isRadioOrCheckbox) {
                            if (isRadioOrCheckbox) {
                                return element.closest('.checkbox_group');
                            }
                        }
                    }
                });
            });
        }
    },
    extended: function() {
        //* iCheck
        if ($('.icheck').length) {
            $('.icheck').iCheck({
                checkboxClass: 'icheckbox_minimal',
                radioClass: 'iradio_minimal'
            }).on('ifChanged', function(event) {
                $('.icheck[name="check"]').parsley('validate');
            });
        }
    }
};
//* chackbox and radio buttons
icheck = {
    init: function() {
        if ($('.icheck').length) {
            $('.icheck').iCheck({
                checkboxClass: 'icheckbox_minimal',
                radioClass: 'iradio_minimal'
            });
        }
    }
};
// Document Ready
$(document).ready(function() {
    icheck.init();
    if (Modernizr.mq('(max-width: 767px)')) {
       $('html').addClass("mobile-menu-js").removeClass("desktop-menu-js");
       } else {
         $('html').addClass("desktop-menu-js").removeClass("mobile-menu-js");
       }

    $('.li-pre > a').click(function(e) {
       if( $('html').hasClass('mobile-menu-js') ) {
         e.preventDefault();
         $(this).parent().find('ul').slideToggle( "slow", function() {
           // Animation complete.
         });
       }
   });
    
       $('.nav li').hover(function() {
        if( $('html').hasClass('desktop-menu-js') ) {
          $(this).find('ul').stop(true,true).fadeIn(); }
          },function(){
            if( $('html').hasClass('desktop-menu-js') ) {
          $(this).find('ul').stop(true,true).hide(); }
      });
    // if( $('.resize-img').length > 0){
    //     $('.resize-img img').resizeToParent(); 
    //     console.log('hini');   
    // }
    if( $('.fancybox-thumbs').length > 0)
    {
        $('.fancybox-thumbs').fancybox({
            prevEffect : 'none',
            nextEffect : 'none',
            padding : 0,
            closeBtn  : true,
            arrows    : true,
            nextClick : true,
            helpers : {
                thumbs : {
                    width  : 90,
                    height : 90
                }
            }
        });
    }
    if( $('.fancybox').length > 0){
        $(".fancybox").fancybox({
            openEffect  : 'elastic',
            closeEffect : 'elastic',
            padding : 0
        });
    }
    if( $('.fancybox-media').length > 0){
        $('.fancybox-media').fancybox({
            openEffect  : 'none',
            closeEffect : 'none',
            padding : 0,
            helpers : {
                media : {}
            }
        });
    }
    $(".lazy").lazyload({
        event : "sporty",
        effect : "fadeIn",
    });
    $(window).bind("load", function() {
        if( $('.lazy').length > 0) {
            var timeout = setTimeout(function() { $(".lazy").trigger("sporty") }, 100);
        }
    });

    if( $('.slickslider').length > 0){
        $('.slickslider').slick({
            dots: false,
            autoplay: true,
            infinite: true,
            fade: false,
            speed: 400,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplaySpeed: 7000,
            cssEase: 'linear',
            responsive: [
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                }
            ]
        });
    }
});

// Window Load
$(window).load(function() {
  $('.linkaatarget').each(function() {
        $(this).find('a').not('.fancybox-thumbs, .fancybox, .fancybox-fade').attr('target', '_blank');
    });
    if( $('.lazy').length > 0){}
    $('#loadingg').fadeOut();
    // windowHeight.init(window);
    // if( $('.parallax-window').length > 0) {
    //     $(".parallax-window").parallax();
    // }
    if( $('.parallax').length > 0) {
        $(".parallax").simpleParallax();
    }
    if( $('.flexslider').length > 0)
    {
        $('.flexslide').flexslider({
            //smoothHeight: true,//animationLoop: true, //startAt: 0,//slideshow: true,
           //slideshowSpeed: 7000,//animationSpeed: 600,//touch: true,//video: false,
           //pauseOnAction: true,//pauseOnHover: false,//prevText: "Previous",//nextText: "Next",
           controlNav: false,
           directionNav: true,
           animation: "fade",
           slideshow: true,
           pauseOnAction: false,
           start: function(){
               $('.panel-collapse').removeClass('in');
               $('.panel:first-child .panel-collapse').addClass('in');
               $('.tab-pane').removeClass('active').removeClass('in');
               $('.tab-pane:first-child').addClass('active').addClass('in');
               var settimer = setTimeout(function(){
                   clearTimeout(settimer);
               }, 300);
               if( $('.parallax-s').length > 0) {
                   $(".parallax-s").simpleParallax();
                   $(".parallax-s").addClass('parallax-s-loaded');
               }
           }
           //sync: "#carousel"
        });
    }
});

// Window Resize
$(window).resize(function(){
    // windowHeight.init(window);
});

// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());
// windowHeight = {
//     init: function(windowi){
//         var windowHeight = $(windowi).outerHeight();
       
//         if (windowHeight < 630) {
//             var contentHeightbox = 630 - 200;
//             $('#content').outerHeight(contentHeightbox);
//         }else {

//             $('#content').outerHeight(contentHeightbox);
//         }
//     }
// }